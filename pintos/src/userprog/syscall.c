#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "userprog/process.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "filesys/directory.h"
#include "devices/shutdown.h"
#include <string.h>

static void syscall_handler (struct intr_frame *);
static void *get_next_arg_address(struct intr_frame *f);
static int get_syscall_number(struct intr_frame *f);
off_t syscall_write(struct intr_frame *f);
void syscall_exit(struct intr_frame *f, int exit_im);
void syscall_halt(void);
int syscall_wait(struct intr_frame *f);
int syscall_exec(struct intr_frame *f);
bool syscall_create(struct intr_frame *f);
bool syscall_remove(struct intr_frame *f);
int syscall_open(struct intr_frame *f);
void syscall_close(struct intr_frame *f);
int syscall_read(struct intr_frame *f);
void syscall_seek(struct intr_frame *f);
unsigned syscall_tell(struct intr_frame *f);
int syscall_size(struct intr_frame *f);

struct lock global_fd_lock;

void free_file_fd_table(struct file **fd_table)
{
	int i;

	for (i = 0; i < MAX_FD_FILE_TABLE_SIZE; i++) {
		if (fd_table[i]) {
			file_close(fd_table[i]);	
			fd_table[i] = NULL;
		}
	}
}

void
syscall_init (void) 
{
    intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

/* This function provides the functionality to write to the console as well
 * as the files. The arguments passed by the user are taken from the frame
 * The file descriptor passed by the user decides whether to write to console 
 * or to the file. If the file descriptor is 1, it writes to the console else
 * the file indexed by that file desciptor. This function returns the number 
 * of bytes written */ 

off_t syscall_write(struct intr_frame *f) 
{
	struct file *file = NULL;
	off_t bytes_written = 0;
	unsigned *size;
	uint32_t *buffer_ptr;
	void *buffer = NULL;
	int *fd;


	/* Write system call has 3 arguements, get the arguemnts in the order fd, buffer, size */

        if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
            fd         = (int *) get_next_arg_address(f);
        }else{
            syscall_exit(f,1);
        }

        if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
             buffer_ptr = (uint32_t *)get_next_arg_address(f);
        }else{
             syscall_exit(f,1);
        }
        if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
             size       = (unsigned *) get_next_arg_address(f);
        }else{
             syscall_exit(f,1);
        }

	/* validate the user buffer */
	buffer = *(void **)buffer_ptr;
	if (!process_validate_user_vaddr((*(void **)buffer_ptr), __func__)) {
		syscall_exit(f, 1);
	} 
	
	/* If fd is 1, write to the console */
	if (*fd == 1) {
		putbuf(buffer, *size);
		bytes_written = (off_t) *size;
	}  else {
			if (*fd < 3 || *fd > MAX_FD_FILE_TABLE_SIZE)
				return -1;
			lock_acquire(&global_fd_lock);
			/* get struct file for the corresponding fd, this is stored
 			 * at the time of open */ 
        	file = thread_current()->fd_file_table[*fd];
            if(file == NULL) {
				lock_release(&global_fd_lock);
		    	return -1;
            }

        	bytes_written = file_write(file, buffer, *size);
			lock_release(&global_fd_lock);
	}

	return bytes_written;
}

/*
 * This function can be called either from syscall_handler() or from other 
 * functions where the user process needs to exit
 * if exit_imm is set, child exit status is set to -1
 *    otherwise, the status is taken from the stack
 */
void syscall_exit(struct intr_frame *f, int exit_imm)
{
	int *status = 0;
	struct thread *t = NULL;
	int old_level;

    t = thread_current();
	/* Allow WRITE to the executable file */
	lock_acquire(&global_fd_lock);
  	if (t->write_dis && t->file) {
  		file_allow_write(t->file);
		t->write_dis = 0;
		t->file = NULL;
 	 }
	lock_release(&global_fd_lock);

	/* Set the child exit status in parent thread
     * if exit_imm is set, the status should be set to -1
     * otherwise the status should come from the stack value
     * Since we are setting the child_exit_status in parent thread and it
     * can exit at any point, need to disable the interrupt until
     * the status is set */
	old_level = intr_disable();
	if (!exit_imm) {
		if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
			status = (int *)get_next_arg_address(f);
		} else {
			*status = CHILD_STATUS_ERROR;
    	}
		t->parent_t->child_status[t->child_id].child_exit_status = (*status < 0)? CHILD_STATUS_ERROR : *status;
    } else {
		t->parent_t->child_status[t->child_id].child_exit_status = CHILD_STATUS_ERROR;
	}
	printf ("%s: exit(%d)\n", t->name,
						t->parent_t->child_status[t->child_id].child_exit_status);
	intr_set_level(old_level);

	/* if the parent is waiting for the child, wake him up */
    lock_acquire(&t->wait_lock);
	cond_signal(&t->wait_cond, &t->wait_lock);
    lock_release(&t->wait_lock);

	thread_exit();
}

void syscall_halt(void)
{
	shutdown_power_off();
}

/* called by syscall_handler to fork a child to load and run a new process */
int syscall_exec(struct intr_frame *f)
{
	char *task;
	void *ptr;
	tid_t tid = TID_ERROR;

	if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
		ptr = (void *)get_next_arg_address(f);
		if (process_validate_user_vaddr(*((void **)ptr), __func__)) {
			task = (char *)(*(void **)ptr);
			tid = exec_process_execute(task);
			/*
 			 * parent has to wait for the child to finish the loading, load_done will be
 			 * set by the child after loading. 	
 			 */ 
			if (tid != TID_ERROR) {
				lock_acquire(&thread_current()->exec_lock);
				if (thread_current()->load_done == -1) {
					cond_wait(&thread_current()->exec_cond, &thread_current()->exec_lock);
				} 
				/* reset the value of load_done so that the parent waits next time when
 				 * it's running exec */
				if (thread_current()->load_done != 1) {
					thread_current()->load_done = -1;
					lock_release(&thread_current()->exec_lock);
					return TID_ERROR;
				}
				thread_current()->load_done = -1;
				lock_release(&thread_current()->exec_lock);
			}
		} else {
			syscall_exit(NULL, 1);
		}
	}

	return tid;
}

int syscall_wait(struct intr_frame *f)
{
	int *child_pid;
	int status = -1;

	if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
		child_pid = (int *)get_next_arg_address(f);
		/* process_wait function has all the logic to handle all the cases
		 * 1. child has already exited
		 * 2. child is still alive
		 * 3. wait is being called multiple times.
		 * 4. wait is called on a child_pid by a process which is not a parent of the child
		 */       
		status = process_wait(*child_pid);
	} 

	return status;
}

/* This function creates a new file by taking the file name and the
 * file size as parameters */
bool syscall_create(struct intr_frame *f)
{   
	char *file_name;
	unsigned *file_size;
	bool success;

	if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
		file_name=*(char **) get_next_arg_address(f);
		if(!process_validate_user_vaddr(file_name, __func__))
			syscall_exit(f,1);
	}else{
		syscall_exit(f,1);
	}

	/* This function validates the address passed by the user to check if
	* it is a valid user address*/
	if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
		file_size = (unsigned *)get_next_arg_address(f);
	}else{
		syscall_exit(f,1);
	}
	lock_acquire(&global_fd_lock);
	success = filesys_create(file_name,*file_size);
	lock_release(&global_fd_lock);

	return success;
}

/* This function opens the file addressed by its file name and
 * if it is valid file name it opens the file else calls exit*/ 
int syscall_open(struct intr_frame *f)
{
	char *file_name;

	/* check for validity of the address, if not valid, do syscall_exit */
	if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
		file_name=*(char **) get_next_arg_address(f);
		if(!process_validate_user_vaddr(file_name, __func__))
         	syscall_exit(f,1);
	} else {
		syscall_exit(f,1);
	}

	/* open the file and save the file pointer in fd_file_table, this would be used
     * to map the struct file to file descriptors
     */ 
	if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
		lock_acquire(&global_fd_lock);
		thread_current()->fd_file_table[++(thread_current()->cur_fd)] = filesys_open(file_name);
		/* if open is successful, return fd otherwise return -1 */
		if(thread_current()->fd_file_table[thread_current()->cur_fd] == NULL) {
			lock_release(&global_fd_lock);
			return -1;
		}
		lock_release(&global_fd_lock);
	} else {
		syscall_exit(f,1);
	}
  
	return thread_current()->cur_fd;
}

/* This function returns the size of the file addressed by its file
 * descriptor */
int syscall_size(struct intr_frame *f)
{
	int *fd;
	off_t  size;
	struct file *file = NULL;

	fd = (int *) get_next_arg_address(f);
	lock_acquire(&global_fd_lock);
	file = thread_current()->fd_file_table[*fd];
	size = file_length(file);
	lock_release(&global_fd_lock);

	return size;
}

/* This function reads from the file into the buffer. The file descriptor is
 *  used to get the structure of the file anf if it is not a valid file then 
 *  exit is called */
int syscall_read(struct intr_frame *f)
{
	struct file *file = NULL;
	off_t bytes_read = 0;
	unsigned *size;
	void *buffer = NULL;
	int *fd; 
	uint32_t *buffer_ptr;
  
	/* validate the addresses and get the arguments from the stack */
	if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
      	fd = (int *) get_next_arg_address(f);

		if (*fd < 3 || *fd > MAX_FD_FILE_TABLE_SIZE) {
			return -1;
		}
	}else{
      	syscall_exit(f,1);
  	}	

  	if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
  		buffer_ptr = (uint32_t *)get_next_arg_address(f);
  	}else{
		syscall_exit(f,1);
 	}
	if (process_validate_user_vaddr(((char*)f->esp) - 4, __func__)) {
      	size      = (unsigned *) get_next_arg_address(f);
  	}else{
      	syscall_exit(f,1);
  	}

	/* get struct file corresponding to the fd */
	lock_acquire(&global_fd_lock);
  	file = thread_current()->fd_file_table[*fd];
	if(file == NULL)
	{
		lock_release(&global_fd_lock);
		return -1;
	}

	/* validate the user buffer */
	buffer = (void *) *buffer_ptr;
	if (!process_validate_user_vaddr(buffer, __func__)) {
		lock_release(&global_fd_lock);
        syscall_exit(f,1);
  	}

	bytes_read = file_read(file,buffer,*size);
	lock_release(&global_fd_lock);

	return bytes_read;
}

/* This function tells the current position in a given file */ 
unsigned syscall_tell(struct intr_frame *f)
{
	int *fd;
	struct file *file = NULL;
	off_t position;
  
	fd   = (int *) get_next_arg_address(f);

	lock_acquire(&global_fd_lock);
	file = thread_current()->fd_file_table[*fd];
	position = file_tell(file);
	lock_release(&global_fd_lock);

	return position;

}

/* This function is used to change the position of the pointer in
 * a given file */ 
void syscall_seek(struct intr_frame *f)
{
	int *fd;
	int *new_position;
	struct file *file = NULL;

	fd   = (int *) get_next_arg_address(f);
	new_position  = (int *) get_next_arg_address(f);
  
	lock_acquire(&global_fd_lock);
	file = thread_current()->fd_file_table[*fd];
	file_seek(file,*new_position);
	lock_release(&global_fd_lock);

	return;
}

/*This function removes the file that was created and if
 * the removal is succesfull, it returns true */
bool syscall_remove(struct intr_frame *f)
{  
	char *file_name;
	bool success;
  
	file_name=*(char **) get_next_arg_address(f);
	lock_acquire(&global_fd_lock);
	success = filesys_remove(file_name);  
	lock_release(&global_fd_lock);
	return success;
}

/* This function closes the file indexed by the file descriptor
 * and the corresponding file is closed */ 
void syscall_close(struct intr_frame *f)
{
	int *fd;
	struct file *file = NULL;

	fd =(int *) get_next_arg_address(f);
	/* validate fd address */
	if (!process_validate_user_vaddr(fd, __func__))
		syscall_exit(NULL, 1);

	/* validate fd */
	if (*fd < 3 || *fd > MAX_FD_FILE_TABLE_SIZE) {
		return;
	}
	lock_acquire(&global_fd_lock);
	file = thread_current()->fd_file_table[*fd];
	if(file == NULL){
		lock_release(&global_fd_lock);
		return;
	}
	file_close(file);
	thread_current()->fd_file_table[*fd]= NULL;  
	lock_release(&global_fd_lock);
	return ;
}

/* system call number is at the current stack pointer
 * This function also updates the esp in intr_frame by 4 bytes */
static int get_syscall_number(struct intr_frame *f)
{
	int *fd;
	fd = (int *) f->esp;
  	f->esp += 4;

	return *fd;
}

/* Returns the  next argument in the stack
 * Also, updates the esp to point to next arguemnt
 */
static void *get_next_arg_address(struct intr_frame *f)
{
	void *argp = f->esp;
	f->esp += 4;

	return argp;
}

/* The syscall_handler handles all the system calls and the
 * corresponding function is called to perform that system call
 * opeation */
static void
syscall_handler (struct intr_frame *f UNUSED) 
{
	int syscall_num;

	if (!process_validate_user_vaddr((char *)f->esp - 4, __func__)) {
        syscall_exit(f, 1);
		return;
	}
		
	/* Get the system call number from the user program's stack */
	syscall_num = get_syscall_number(f);

	/* Call System call handelr functions */
	switch (syscall_num) {
		case SYS_WRITE:
			f->eax = syscall_write(f);
			break;
		case SYS_EXIT:
			syscall_exit(f, 0);
			break;
		case SYS_HALT:
			syscall_halt();
			break;
		case SYS_EXEC:
			f->eax = syscall_exec(f);
			break;
		case SYS_WAIT:
			f->eax = syscall_wait(f);
			break;
                case SYS_CREATE:
                        f->eax=syscall_create(f);
                        break;
                case SYS_OPEN:
                        f->eax=syscall_open(f);
                        break;
                case SYS_FILESIZE:
                        f->eax=syscall_size(f);
                        break;
                case SYS_READ:
                       f->eax=syscall_read(f);
                       break;
                case SYS_TELL:
                       f->eax=syscall_tell(f);
                       break;
                case SYS_SEEK:
                      syscall_seek(f);
                      break;
                case SYS_REMOVE:
                      f->eax=syscall_remove(f);
                      break;
                case SYS_CLOSE:
                      syscall_close(f);
                      break;
		default:
  			printf ("system call! num=%d\n", syscall_num);
  			thread_exit ();
	}
}

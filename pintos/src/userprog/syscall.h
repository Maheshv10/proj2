#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include "threads/interrupt.h"
#include "filesys/file.h"

void syscall_init (void);
void syscall_exit(struct intr_frame *f, int exit_im);
void free_file_fd_table(struct file **fd_table);

#endif /* userprog/syscall.h */
